// create express app
const express = require('express');
const app = express();
const dotenv = require('dotenv')

dotenv.config()
const bodyParser = require('body-parser');

const port = process.env.PORT || 5000;
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
    // define a root route
app.get('/', (req, res) => {
    res.send("Hello World");
});
app.use(function(req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
const usersRoutes = require('./src/routes/users.routes');
app.use('/api/v1/myquestionauth', usersRoutes)




app.listen(port, () => {
    console.log(`L'Application MyQuestion-back-authentificateur à l'écoute sur le port ${port}!`)
});
