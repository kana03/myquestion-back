const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const userController = require('./users.controller');
const dotenv = require('dotenv');
dotenv.config();

exports.login = function(req, res) {
    try {
        const email = req.body.email;
        const password = req.body.password;
        if (!(email && password)) {
            res.status(400).send("All input is required");
        }
        return new Promise((resolve, reject) => {
            userController.findByEmail(email, function(err, res) {
                reject(err, null);

            }).then(user => {
                console.log(user);

                if (!user) {
                    res.statut(401).send("This user does not exit!");
                } else {
                    const userParse = JSON.parse(JSON.stringify(user));
                    if (user && (bcrypt.compare(password, user.password))) {
                        const token = jwt.sign(userParse, process.env.TOKEN_KEY, {
                            expiresIn: "2h"
                        })
                        console.log(token);
                        userParse.token = token;
                        resolve(res.send(userParse));
                    } else {
                        console.log(res.statusCode);
                    }
                }
            });
        })
    } catch (err) {
        console.log(err);
    }
};