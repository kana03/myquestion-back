'use strict';

const bcrypt = require('bcryptjs');
const { dbConn } = require('../config/db.config');

//User object create
var Users = function(user) {
    this.email = user.email;
    this.password = user.password;
    this.pseudo = user.pseudo;
};
Users.create = function(newUser, result) {
    // Validate user input
    if (!(newUser.email && newUser.password && newUser.pseudo)) {
        res.status(400).send("All input is required");
    }

    //Encrypt user password
    var encryptedPassword = bcrypt.hash(newUser.password, 10);

    // Create user in our database

    newUser.email = newUser.email.toLowerCase(), // sanitize: convert email to lowercase
        newUser.password = encryptedPassword,

        dbConn.query("INSERT INTO users set ?", newUser, function(err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
            } else {
                result(null, res.insertId);
            }
        });
};

Users.findById = function(id, result) {
    dbConn.query("Select * from users where id = ? ", id, function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else { result(null, res); }
    });
};

Users.findByEmail = function(email) {
    return new Promise((resolve, reject) => {
        dbConn.query("select * from users where email = ? ", email, function(err, res) {
            if (err) {
                console.log("error: ", err);
                reject(err, null);
            } else {
                resolve(res[0]);
            }
        });
    })


};

Users.findAll = function(result) {
    dbConn.query("SELECT * FROM users", function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log('users : ', res);
            result(null, res);
        }
    });
};
Users.update = function(id, user, result) {
    dbConn.query("UPDATE users SET email=?,password=?,pseudo=?WHERE id = ?", [user.email, user.password, user.pseudo, id], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else { result(null, res); }
    });
};
Users.delete = function(id, result) {
    dbConn.query("DELETE FROM users WHERE id = ?", [id], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else { result(null, res); }
    });
};
module.exports = Users;
