const express = require('express')

const router = express.Router()
const userController = require('../controllers/users.controller');
const loginController = require('../controllers/login.controller');
// const Middleware        = require("../middleware/middleware");

// Retrieve all Users
router.get('/', userController.findAll);

// Create a new user
router.post('/register', userController.create);



// lo a new user
router.post('/login', loginController.login);

// Retrieve a single user with id
router.get('/:id', userController.findById);
// Retrieve a single user with email
router.get('/get/:email', userController.findByEmail);

// Update a user with id
router.put('/:id', userController.update);

// Delete a user with id
router.delete('/:id', userController.delete);

module.exports = router